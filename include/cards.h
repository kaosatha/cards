/*
 * HEADER FOR CARDS LIB
 *
 * GABRIEL RODGERS // KAOSATHA
 *
 * BSD 2-Clause License
 */


#ifndef CARDS_H_

#define CARDS_H_
#define DECK_SIZE 52
#define CARD_MESSAGE_MAX 22

#include <err.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


/* suit: the 4 possible nums for a card's suit */
enum suit {
	SPADES,
	CLUBS,
	HEARTS,
	DIAMONDS
};

/* nums: valid vals for cards */
enum num {
	ACE = 1,
	TWO,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	TEN,
	NINE,
	JACK,
	QUEEN,
	KING
};

/* card: the suit of a card, and its number */
struct card {
	enum suit suit;
	enum num num;
};

/* deck: a collection of cards */
struct deck {
	struct card **deck;
	unsigned int length;
};

extern struct card  *card_new(enum suit, enum num);
extern void          card_discard(struct card *);
extern int           card_compare(const struct card *, const struct card *);
extern struct card  *card_rand_draw(struct card **);
extern struct card  *card_first_draw(struct card **);
extern struct card  *card_rand_peek(struct card **);
extern struct card  *card_first_peek(struct card **);
extern char         *card_prettify(char *, struct card *, size_t);
extern int           card_write_out(FILE *, struct card *);
extern int           card_print(struct card *);
extern struct card **deck_init(struct card **);
extern int           deck_init_multi(struct card **, unsigned int);
extern struct card **deck_shuffle(struct card **);
extern size_t        deck_size(struct card **);
extern struct card **deck_sort(struct card **);

#endif

