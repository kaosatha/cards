/*
 * CURCARD.H - Header for curses functions for cards
 *
 * GABRIEL RODGERS // KAOSATHA
 *
 * BSD 2-Clause
 */

#ifndef CURCARD_H_
#define CURCARD_H_

#include <ncurses.h>
#include <stdio.h>
#include <string.h>
#include "ascii-table.h"
#include "cards.h"

#define CARDHEIGHT 6
#define CARDWIDTH 7

struct column {
	/* y pos of top corner of pile */
	unsigned short int y;
	/* x pos of top corner of pile */
	unsigned short int x;
	/* ptr index to top card of pile. ranger: 0 -> */
	unsigned short int ptr;
	/* botptr: index to the first uncovered card. range: 1 -> ptr. */
	unsigned short int botptr;
	/* pile of cards. pile[0] is always NULL */
	struct card **pile;
};

extern void cur_c_print(int, int, const char *);
extern void cur_c_print_colour(int, int, const char *, enum suit);
extern void col_print_compact(struct column *);
extern void col_print_long(struct column *);
extern void col_print_long_hint(struct column *, unsigned short int);

unsigned short int cur_bold_f, cur_colour_f, cur_hint_f;

#endif

