## Makefile

.PHONY: all clean install uninstall
.SUFFIXES: .c .o

include mk.conf

$(BINTARGET): dirs $(LIBTARGET)
	$(CC) $(CFLAGS) $(INCLUDE) $(LIBS) -o $@ $(SRC)main.c

$(LIBTARGET): dirs $(LSRC)
	$(CC) $(CFLAGS) $(INCLUDE) -shared -o $@ $(LSRC)

.c.o:
	$(CC) $(CFLAGS) $(INCLUDE) -c -o $@ $<

all: $(BINTARGET)

clean:
	rm -rf $(LIB) $(BIN) $(SRC)lib*.o $(OBJ)

dirs:
	mkdir -p $(BIN)
	mkdir -p $(LIB)

install:
	install -D $(BINTARGET) $(DESTDIR)$(PREFIX)$(BIN)/cards
	install -D $(LIBTARGET) $(DESTDIR)$(PREFIX)$(LIB)/libcursino.so
	mkdir -pm 775 $(DESTDIR)$(PREFIX) /var/games/cursino/
	chown root:games /var/games/cursino
	mkdir -p $(DESTDIR)$(PREFIX)/include/
	install -D ./include/ascii-table.h ./include/cards.h \
	    ./include/curcard.h $(DESTDIR)$(PREFIX)include/
	mkdir -p $(DESTDIR)$(PREFIX)share/man/man0/
	mkdir -p $(DESTDIR)$(PREFIX)share/man/man3/
	mkdir -p $(DESTDIR)$(PREFIX)share/man/man6/
	install -D ./doc/*.0 $(DESTDIR)$(PREFIX)share/man/man0/
	install -D ./doc/*.3 $(DESTDIR)$(PREFIX)share/man/man3/
	install -D ./doc/cards.6 $(DESTDIR)$(PREFIX)share/man/man6/

uninstall:
	rm -f \
	    $(DESTDIR)$(PREFIX)$(BIN)/cards \
	    $(DESTDIR)$(PREFIX)$(LIB)/libcursino.so \
	    $(DESTDIR)$(PREFIX)include/cards.h \
	    $(DESTDIR)$(PREFIX)include/curcard.h \
	    $(DESTDIR)$(PREFIX)include/ascii-table.h \
	    $(DESTDIR)$(PREFIX)share/man/man0/ascii-table.h.0 \
	    $(DESTDIR)$(PREFIX)share/man/man0/cards.h.0 \
	    $(DESTDIR)$(PREFIX)share/man/man0/curcard.h.0 \
	    $(DESTDIR)$(PREFIX)share/man/man3/card.3 \
	    $(DESTDIR)$(PREFIX)share/man/man3/deck.3 \
	    $(DESTDIR)$(PREFIX)share/man/man6/cards.6
