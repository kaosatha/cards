/*
 * SOLITAIRE GENERIC - Functions shared between types of solitaire
 *
 * To use this file correctly, the following should be implemented in the
 * game #including this:
	static void sl_auto_move(void);
	static int sl_card_to_pile(int, int, int);
	static int sl_check_win(void);
	static void sl_clean_board(void);
	static void sl_draw_card(void);
	static int sl_init_board(struct card **);
	static void sl_redraw_board(void);
	static void sl_usage(void);
 * see src/solitaire.c for a "model" implementation
 *
 * GABRIEL RODGERS // KAOSATHA
 *
 * BSD 2-Clause
 */


static int sl_main_internal(struct card **);
static char sl_get_input(int *);
static int sl_move_single(struct column *, struct column *);
static int sl_move_subpile(struct column *, struct column *, unsigned short int);
static void sl_process_input(struct move *, char);
static void sl_usage(void);


int
sl_main_internal(struct card **cs)
{
	int ch, moves, retval, score, type, undo_count, win;
	time_t initial;
	struct move m;

	if (sl_init_board(cs) < 0)
		return -1;
	initial = time(NULL);

	m.src = m.dst = m.amount = 0;
	moves = undo_count = win = 0;

	while ((type = sl_get_input(&ch))) {
		sl_redraw_board();
		switch (type) {
		case 'm':
			++moves;
			sl_auto_move();
			m.amount = 0;
			break;
		case 'd':
			sl_draw_card();
			m.amount = 0;
			break;
		case 'q':
			goto _endgame;
		case 'u':
			if (sl_undo_move(&m) >= 0) {
				--moves;
				++undo_count;
				m.amount = 0;
			}
			break;
		case NUM:
			sl_process_input(&m, ch);
			retval = sl_card_to_pile(m.dst, m.src, m.amount);
			move(1, 0);
			clrtoeol();
			/*
			 * m.amount is zero'd on error so no cards get moved if
			 * undo is called with bad values
			 */
			if (retval < 0) {
				printw("Input out of range - ? for help.");
				m.amount = 0;
			} else if (retval > 0) {
				printw("Move is invalid - ? for help.");
				m.amount = 0;
			} else {
				printw("Moving %d %s to pile %d from pile %d. ",
				    m.amount, (m.amount==1 ? "card" : "cards"),
				    m.dst, m.src);
				++moves;
			}
			break;
		case '?':
			sl_usage();
			break;
		default:
			move(1,0);
			clrtoeol();
			printw("Invalid key pressed - ? for help.");
			break;
		}
		sl_redraw_board();
		if (!sl_check_win()) {
			win = 1;
			goto _endgame;
		}
	}
_endgame:
	move(1, 0);
	clrtoeol();
	score = time(NULL) - initial;
	attron(A_BOLD);
	printw("You %s, taking %d seconds. ", win ? "Won" : "Lost", score);
	printw("Moves: %d. Undo's: %d.", moves, undo_count);
	attroff(A_BOLD);
	refresh();
	getch();
	sl_clean_board();
	return win ? score : 0;
}


/* sl_get_input: helper function for getting in-game input */
char
sl_get_input(int *ch)
{
	*ch = getch();

	/* ':' used as a leader char for 2 digit numbers */
	if (isdigit(*ch) || *ch == ':')
		return NUM;
	return *ch;
}


/* sl_move_subpile: moves a subpile from stack to stack */
int
sl_move_subpile(struct column *dst, struct column *src, unsigned short int n)
{
	struct column tmp;
	struct card *cs[30];

	if (!dst || !src || !n || src->ptr < n)
		return -1;

	tmp.ptr=0;
	tmp.botptr=0;
	tmp.pile = cs;
	tmp.pile[0] = NULL;

	for (; n>0; --n)
		sl_move_single(&tmp, src);
	while (tmp.ptr > 0)
		sl_move_single(dst, &tmp);
	return 0;
}


/*sl_move_single: moves a single card from src to dst, updating ptr info */
int
sl_move_single(struct column *dst, struct column *src)
{
	if (!dst || !src || src->ptr == 0)
		return -1;

	dst->pile[++(dst->ptr)] = src->pile[(src->ptr)--];

	/*
	 * issue with card pile reaching bottom and bottom ptr being 0 not 1.
	 * this deals with that
	 */
	if (dst->botptr == 0)
		dst->botptr++;
	/* keep ptr and botptr aligned if pile completely empty */
	if (src->ptr == 0)
		src->botptr = 1;
	else if (src->ptr < src->botptr)
		src->botptr--;
	return 0;
}


/* sl_process_input: fills `m` with values. m->src becomes numeric ch */
void
sl_process_input(struct move *m, char ch)
{
	unsigned short int n, n1, n2;

	move(1, 0);
	clrtoeol();
	if (ch == ':') {
		n1 = getch() - '0';
		n2 = getch() - '0';
		m->src = n1*10 + n2;
	} else
		m->src = ch - '0';
	printw("From %d.\t", m->src);
	refresh();
	if ((n = getch()) == ':') {
		n1 = getch() - '0';
		n2 = getch() - '0';
		m->amount = n1*10 + n2;
	} else
		m->amount = n - '0';
	printw("Amount: %d.", m->amount);
	refresh();
	if ((n = getch()) == ':') {
		n1 = getch() - '0';
		n2 = getch() - '0';
		m->dst = n1*10 + n2;
	} else
		m->dst = n - '0';
	printw("To: %d\t", m->dst);
	refresh();
}

/* sl_usage: display help sheet */
void
sl_usage(void)
{
	move(1, 0);
	clrtobot();
	printw("HOW TO PLAY\n"
	    "d - Draw a card/deal.\n"
	    "u - Undo last move. Only remembers one move.\n"
	    "m - \n"
	    "\nMOVING CARDS\n"
	    "Cards are moved by entering three numbers:\n"
	    "\t the pile to move from\n"
	    "\t the amount of cards to move\n"
	    "\t the pile to move them too.\n"
	    "double digit numbers must be proceeded by a colon\n"
	    "examples:\n"
	    "137 - 3 cards from pile 1 to 7\n"
	    "4:105 - 10 cards from pile 4 to pile 5\n"
	    "If a game has a tenth pile, it can be accessed using '0'\n"
	    "Otherwise, pile 0 refers to the draw pile.\n");
	getch();
}

