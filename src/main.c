/*
 * CARDS
 *
 * GABRIEL RODGERS // KAOSATHA
 *
 * BSD 2-Clause
 */

#include <sys/stat.h>
#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <ncurses.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "ascii-table.h"
#include "blackjack.h"
#include "cards.h"
#include "solitaire.h"


#define SCOREPATH "/var/games/cursino/"

static void init_curses(int);
static void usage(int);
static int write_score(const char *, const char *, int);


/* main: this program does stuff with cards */
int
main(int argc, char **argv)
{
	int (*game)(void);

	char *game_name;
	int ch, score;

	initscr();
	if (unveil(SCOREPATH, "wxc") < 0)
		err(EXIT_FAILURE, "unveil");
	if (pledge("stdio rpath wpath cpath tty", NULL) < 0)
		err(EXIT_FAILURE, "pledge");

	game = NULL;
	while ((ch = getopt(argc, argv, "BbcHhS:s")) != -1) {
		switch (ch) {
		case 'h':
			endwin();
			usage(EXIT_SUCCESS);
		case 'H':
			cur_hint_f = 1;
			/* fallthrough: -H implies -B*/
		case 'B':
			cur_bold_f = 1;
			break;
		case 'c':
			cur_colour_f = 1;
			break;
		case 'b':
			game = &bj_main;
			game_name = "blackjack";
			break;
		case 'S':
			if (!strcmp("1", optarg))
				ss_difficulty = 1;
			else if (!strcmp("2", optarg))
				ss_difficulty = 2;
			else if (!strcmp("4", optarg))
				ss_difficulty = 4;
			else {
				warnx("invalid argument given to `-S`");
				usage(EXIT_FAILURE);
			}
			game = &ss_main;
			game_name = "spider";
			break;
		case 's':
			game = &sx_main;
			game_name = "solitaire";
			break;
		default:
			endwin();
			usage(EXIT_FAILURE);
		}
	}

	if (!game) {
		game_name = "blackjack";
		game = &bj_main;
	}


	init_curses(cur_colour_f);
	printw("Welcome to the Cursino! Today you are playing %s.", game_name);
	refresh();
	score = (*game)();
	endwin();
	if (score > 0)
		if (write_score(SCOREPATH, game_name, score) < 0)
			fprintf(stderr, "error recording score\n");
	return 0;
}


/* init_curses: sets some settings for ncurses */
void
init_curses(int cf)
{
	if (cf && has_colors() && start_color() >= 0) {
		init_pair(0, COLOR_WHITE, COLOR_BLACK);
		init_pair(1, COLOR_WHITE, COLOR_BLACK);
		init_pair(2, COLOR_RED, COLOR_BLACK);
		/*init_pair(1, COLOR_BLACK, COLOR_WHITE); */
		/* init_pair(2, COLOR_RED, COLOR_WHITE); */
	}
	raw();
	noecho();
	curs_set(0);
}


/* usage: prints a help message, and other info */
void
usage(int ret)
{
	fprintf(stderr, "cards: written by Kaosatha.\n"
	    "ASCII-art created by llizard aka ejm."
	    " - https://llizardakaejm.wordpress.com/\n\n"
	    "USAGE:"
	    "\n-B\tenable bold card art."
	    "\n-c\tenable colours if your terminal supports them."
	    "\n-H\tenable bold hinting for movable piles (implies -B)."
	    "\n-b\tplay blackjack (default)."
	    "\n-S num"
	    "\n\tplay spider solitaire with `num` suits, where `num` is 1, 2, "
	    "\n\tor 4."
	    "\n-s\tplay solitaire."
	    "\n-h\tprint this help\n");
	exit(ret);
}


/* write the score to the file `game_name` in the directory `path` */
int
write_score(const char *path, const char *game_name, int score)
{
	char *fname, *user;
	FILE *f;

	if ((fname = malloc(strlen(path) + strlen(game_name) + 2)) == NULL)
		return -1;
	sprintf(fname, "%s/%s", path, game_name);

	if ((f = fopen(fname, "a")) == NULL) {
		warn("could not open %s/%s", path, game_name);
		free(fname);
		return -1;
	}
	user = getenv("USER");
	if (user != NULL && *user != '\0')
		fprintf(f, "%d\t%s\n", score, user);
	else
		fprintf(f, "%d\tAnonymous\n", score);
	free(fname);
	fclose(f);
	return 0;
}

