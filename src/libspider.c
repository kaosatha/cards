/*
 * SPIDER SOLITAIRE - Part of the Cursino
 *
 * GABRIEL RODGERS // KAOSATHA
 *
 * BSD 2-Clause
 */

#include <stdio.h>
#include <stdlib.h>

#include "solitaire.h"

#define DECKSIZE 105 /* 2 decks of 52 + final NULL */

static void sl_auto_move(void);
static int sl_card_to_pile(unsigned int, unsigned int, unsigned int);
static int sl_check_win(void);
static void sl_clean_board(void);
static void sl_draw_card(void);
static int sl_init_board(struct card **);
static void sl_redraw_board(void);
static int sl_undo_move(struct move *);
static int ss_deck(struct card **cs, int difficulty);
static unsigned int ss_get_chain(struct column *);


/* TODO: will need to look properly into what to do for final destination */
enum INDEX {draw=0, first, second, third, fourth, fifth, sixth, seventh, eighth,
    ninth, tenth};
static struct column *_draw, *_first, *_second, *_third, *_fourth, *_fifth;
static struct column *_sixth, *_seventh, *_eighth, *_ninth, *_tenth;
static struct column **COLD;

 /*
 * XXX IMPORTANT - IMPORTANTE - WICHTIG XXX
 * this include is important. it contains
 *	sl_main_internal
 *	sl_move_single
 *	sl_move_subpile
 *	sl_get_input
 *	sl_process_input
 *	sl_usage
 * think of it as spider inherits from and overloads solitaire-generic
 * XXX IMPORTANT - IMPORTANTE - WICHTIG XXX
 */
#include "solitaire-generic.c"


int
ss_main(void)
{
	int retval;
	struct card **cs;

	if ((cs = calloc(sizeof(struct card *), DECKSIZE)) == NULL)
		return -1;
	if (ss_deck(cs, ss_difficulty) < 0)
		return -1;

	retval = sl_main_internal(cs);
	while (*cs != NULL)
		free(*cs++);
	free(cs);
	return retval;
}


void
sl_auto_move(void)
{
	unsigned short int i;

	for (i=first; i<=tenth; ++i) {
		if (ss_get_chain(COLD[i]) == 13) {
			COLD[i]->ptr -= 13;
			COLD[i]->botptr =-
			    COLD[i]->ptr < COLD[i]->botptr ? 1 : 0;
		}
	}
}


int
sl_card_to_pile(unsigned int dst, unsigned int src, unsigned int n)
{
	enum num dstn, srcn;
	unsigned short int sb_ptr, valid;

	/* treat 0 as a 10 on input */
	src = (src == 0) ? 10 : src;
	dst = (dst == 0) ? 10 : dst;
	/* dst and src line up with column values fine, no conversion needed */
	if (src < 1 || src > 10 || dst < 1 || dst > 10)
		return -1;
	if (n > COLD[src]->ptr || !n)
		return -1;

	/* helper variable */
	sb_ptr = COLD[src]->ptr - (n-1);

	/* check if src can actually be moved on to dst */
	valid = 0;
	if (COLD[dst]->ptr == 0)
		valid = 1;
	else {
		dstn = COLD[dst]->pile[COLD[dst]->ptr]->num;
		srcn = COLD[src]->pile[sb_ptr]->num;
		if (srcn+1 == dstn)
			valid = 1;
	}

	switch (n) {
	case 1:
		if (valid) {
			sl_move_single(COLD[dst], COLD[src]);
			return 0;
		}
		break;
	default:
		/* check the subpile to make sure it can be moved */
		if (ss_get_chain(COLD[src]) < n)
			return 1;
		if (valid) {
			sl_move_subpile(COLD[dst], COLD[src], n);
			return 0;
		}
		break;
	}
	return 1;
}


int
sl_check_win(void)
{
	int i;

	for (i=first; i<=tenth; ++i)
		if (COLD[i]->ptr != 0)
			return -1;
	return 0;
}

void
sl_clean_board(void)
{
	int i;

	for (i=first; i<=tenth; i++) {
		free(COLD[i]->pile);
		free(COLD[i]);
	}
	free(COLD);
}


void
sl_draw_card(void)
{
	size_t i;

	for (i=first; i<=tenth; ++i)
		sl_move_single(COLD[i], COLD[draw]);

}


int
sl_init_board(struct card **cs)
{
	int i, lim, j;

	if ((COLD = calloc(sizeof (struct col *), 11))) {
		COLD[draw] = _draw;
		COLD[first] = _first;
		COLD[second] = _second;
		COLD[third] = _third;
		COLD[fourth] = _fourth;
		COLD[fifth] = _fifth;
		COLD[sixth] = _sixth;
		COLD[seventh] = _seventh;
		COLD[eighth] = _eighth;
		COLD[ninth] = _ninth;
		COLD[tenth] = _tenth;
	} else
		return -1;

	for(i=first; i<=tenth; ++i) {
		if ((COLD[i] = malloc(sizeof (struct column))) == NULL)
			return -1;
		if (!(COLD[i]->pile = calloc(sizeof (struct card *), 50)))
			return -1;

		COLD[i]->pile[0] = NULL;
		COLD[i]->y = 10;
		COLD[i]->x = (i - 1) * CARDWIDTH;

		/*
		 * fill piles with approprate amount of cards based on solitaire
		 * board. the first 4 piles have 6 cards in, the rest have 5
		 */
		lim = (i<=4 ? 6 : 5);
		for (j=1; j <= lim; ++j) {
			COLD[i]->pile[j] = card_rand_draw(cs);
			if (COLD[i]->pile[j] == NULL)
				return -1;
		}
		COLD[i]->ptr = j-1;
		COLD[i]->botptr = j-1;
	}

	deck_shuffle(cs);

	if ((COLD[draw] = malloc(sizeof (struct column))) == NULL)
		return -1;

	COLD[draw]->ptr = deck_size(cs);
	COLD[draw]->botptr = COLD[draw]->ptr;
	COLD[draw]->pile = cs;
	COLD[draw]->y = 2;
	COLD[draw]->x = 0;

	/* TODO: fix this so it works properly */
	/* swap the first card with the terminating NULL */
	COLD[draw]->pile[COLD[draw]->ptr] = COLD[draw]->pile[0];
	COLD[draw]->pile[0] = NULL;

	return 0;
}


void
sl_redraw_board(void)
{
	char *s;
	int i, n, total_cards, y_pos;

	move(2, 0);
	clrtobot();

	for (i=first, total_cards=0; i<=tenth; ++i) {
		mvprintw(9, COLD[i]->x + 3, "%d", i);
		n = ss_get_chain(COLD[i]);
		if (cur_hint_f)
			col_print_long_hint(COLD[i], n);
		else
			col_print_long(COLD[i]);
		y_pos = COLD[i]->y + (COLD[i]->ptr * 2) + CARDHEIGHT;
		mvprintw(y_pos, COLD[i]->x+3, "%d", n);
		total_cards += COLD[i]->ptr;
	}

	for (i=0; i<total_cards; ++i) {
		cur_c_print(2, 10+(i*2), NULL);
	}

	if (COLD[draw]->ptr != 0)
		s = " _____ \n|     |\n| Card|\n|Draw |\n| Pile|\n|_____|";
	else
		s = " _____ \n|     |\n|     |\n|     |\n|     |\n|_____|";
	cur_c_print_colour(COLD[draw]->y, COLD[draw]->x, s, 1);
	refresh();
	return;
}


int
sl_undo_move(struct move *m)
{
	if (m->amount == 0)
		return -1;
	sl_move_subpile(COLD[m->src], COLD[m->dst], m->amount);
	COLD[m->src]->botptr += (COLD[m->src]->botptr == 1) ? 0 : 1;
	move(1, 0);
	clrtoeol();
	printw("Undid last move");
	m->amount = 0;
	return 0;
}


/* ss_deck: create a spider deck based on difficulty and save in cs */
int
ss_deck(struct card **cs, int difficulty)
{
	enum num n;
	enum suit s;
	int i, k;
	struct card *c;

	srand(time(NULL));

	for (i=0, k=0; i<8; ++i) {
		switch (difficulty) {
		case 1:
			s = SPADES;
			break;
		case 2:
			s = i & 1 ? SPADES : HEARTS;
			break;
		default:
			s = i % 4;
		}
		for (n=ACE; n<=KING; ++n, ++k) {
			if ((c = card_new(s, n)) == NULL)
				return -1;
			cs[k]=c;
		}
	}
	cs[++k] = NULL;
	return 0;
}



/* ss_get_chain: returns how many consective cards are on the bottom of col */
unsigned int
ss_get_chain(struct column *col)
{
	struct card *curr;
	unsigned short int i;

	if (col == NULL)
		return 0;
	else if (col->ptr <= 1)
		return col->ptr;

	for (i=col->ptr; i>col->botptr; --i) {
		curr = col->pile[i];

		if (curr == NULL)
			break;
		if (col->pile[i-1]->suit == curr->suit
		    && col->pile[i-1]->num == curr->num+1)
		{
			continue;
		} else
			break;
	}
	return col->ptr - i + 1;
}

