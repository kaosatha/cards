/*
 * CURCARD.C - Curses functions for cards
 *
 * GABRIEL RODGERS // KAOSATHA
 *
 * BSD 2-Clause
 */


#include "curcard.h"
#define COLOURLESS 5

/* cur_c_print: prints a cards ascii without colour */
void
cur_c_print(int y, int x, const char *card_text)
{
	cur_c_print_colour(y, x, card_text, COLOURLESS);
}


/* cur_c_print_colour: prints a cards ascii with colour */
void
cur_c_print_colour(int y, int x, const char *card_text, enum suit suit)
{
	char *line, text[53];
	int attrs;
	unsigned short int c_pair, i;

	if (card_text == NULL)
		return;

	switch (suit) {
	case SPADES:
		/* fallthrough */
	case CLUBS:
		c_pair = 1;
		break;
	case HEARTS:
		/* fallthrough */
	case DIAMONDS:
		c_pair = 2;
		break;
	default:
		c_pair = 0;
		break;
	}

	strlcpy(text, card_text, 53);
	line = strtok(text, "\n");
	attrs = cur_bold_f ? A_BOLD : 0;
	attrs |= cur_colour_f ? COLOR_PAIR(c_pair) : 0;
	for (i=0; i<CARDHEIGHT; ++i) {
		attron(attrs);
		mvprintw(y+i, x, "%s", line);
		attroff(attrs);
		line = strtok(NULL, "\n");
	}
}


/* col_print_compact: print the top card of a column at the columns (y, x) */
void
col_print_compact(struct column *col)
{
	char temp[53];
	enum suit suit;
	unsigned short int t_ptr;

	t_ptr = col->ptr;
	suit = t_ptr > 0 ? col->pile[t_ptr]->suit : 0;
	/* need this as cur_c_print_colour uses strtok */
	strlcpy(temp, ascii_get(col->pile[t_ptr]), 53);
	cur_c_print_colour(col->y, col->x, temp, suit);
}


/* col_print_long: prints a pile of card in a vertical column */
void
col_print_long(struct column *col)
{
	col_print_long_hint(col, col->ptr);
}


/* col_print_long_hint: prints col in long format - last n card highlighted */
void
col_print_long_hint(struct column *col, short unsigned int n)
{
	enum suit suit;
	unsigned short int b_ptr, i, old_bold, t_ptr, x_pos, y_pos;

	b_ptr = col->botptr;
	t_ptr = col->ptr;
	y_pos = col->y;
	x_pos = col->x;

	/*
	 * every card up until b_ptr is face down and unknown, so we
	 * print blanks until that
	 *
	 * the numbers in these loops have been worked out based on what looks
	 * good when printed.
	 *
	 * TODO: way to print revealed cards 3 lines apart
	 */
	for (i=1; i<b_ptr; ++i) {
		cur_c_print(y_pos+i*2-2, x_pos, ascii_get(NULL));
	}
	for (; i<=t_ptr; ++i) {
		i > 0 ? (suit = col->pile[i]->suit) : (suit = 0);
		if (t_ptr - i < n) {
			cur_c_print_colour(y_pos+i*2-2, x_pos,
			    ascii_get(col->pile[i]), suit);
		} else {
			old_bold = cur_bold_f;
			cur_bold_f = 0;
			cur_c_print_colour(y_pos+i*2-2, x_pos,
			    ascii_get(col->pile[i]), suit);
			cur_bold_f = old_bold;
		}
	}
}

